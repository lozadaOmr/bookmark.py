from flask import Flask, request
app = Flask(__name__)

@app.route('/')
def api_root():
    return 'Welcome'

@app.route('/bookmarks')
def api_bookmarks():
    if 'url' in request.args:
        return 'Bookmark of ' + request.args['url']
    else:
        return 'Display a listing of bookmarks'

@app.route('/bookmarks', methods = ['POST', 'PUT', 'DELETE'])
def api_echo():
    if request.method == 'POST':
        return "Posting URL:\n" + request.args['url']

    elif request.method == 'PUT':
        return "Updating URL:\n" + request.args['url']

    elif request.method == 'DELETE':
        return "Deleting URL:\n" + request.args['url']

if __name__ == '__main__':
    app.run()